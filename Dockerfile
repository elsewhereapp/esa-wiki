########Maven Image########
FROM maven:3.8.4-jdk-11
WORKDIR /root

# Install Maven dependencies

# Install the ESA score package
RUN git clone https://gitlab.com/elsewhereapp/esa-score esa-score
RUN mvn -f esa-score/pom.xml clean install -Dmaven.test.skip

# Install the ESA core package
RUN git clone https://gitlab.com/elsewhereapp/esa-core esa-core
RUN mvn -f esa-core/pom.xml clean install -Dmaven.test.skip

#copy the code
COPY src /root/src
COPY esa.sh /root
COPY pom.xml /root
COPY README.md /root
COPY LICENSE /root

#run the app
CMD /bin/bash

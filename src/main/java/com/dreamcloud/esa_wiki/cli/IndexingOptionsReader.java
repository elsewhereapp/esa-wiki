package com.dreamcloud.esa_wiki.cli;

import com.dreamcloud.esa_config.cli.OptionsReader;
import com.dreamcloud.esa_wiki.IndexingOptions;

import java.io.File;
import java.util.Properties;
import java.util.Set;

public class IndexingOptionsReader extends OptionsReader {
    private static String SOURCE_DIR = "indexing.sourceDirectory";
    private static String OUTPUT_DIR = "indexing.outputDirectory";
    private static String WIKI_SOURCE = "indexing.source";
    private static String REPEAT_LINKS = "indexing.repeat.links";
    private static String REPEAT_TITLES = "indexing.repeat.titles";
    private static String RARE_TERM_THRESHOLD = "indexing.rare.threshold";
    private static String THREAD_COUNT = "indexing.threads.count";
    private static String BATCH_SIZE = "indexing.threads.batchSize";

    public IndexingOptions getOptions(Properties properties) {
        IndexingOptions options = new IndexingOptions();

        if (hasProperty(properties, SOURCE_DIR)) {
            options.setSourceDirectory(new File(properties.getProperty(SOURCE_DIR)));
        }
        if (hasProperty(properties, OUTPUT_DIR)) {
            options.setOutputDirectory(new File(properties.getProperty(OUTPUT_DIR)));
        }
        if (hasProperty(properties, WIKI_SOURCE)) {
            options.setWikiSource(properties.getProperty(WIKI_SOURCE));
        }

        Set<String> propertyNames = properties.stringPropertyNames();
        propertyNames.stream().sorted().forEach((String property) -> {
            if (property.matches("^indexing\\.stage\\.[0-9]+$")) {
                options.getStages().add(properties.getProperty(property));
            }
        });

        if (hasProperty(properties, REPEAT_LINKS)) {
            options.setRepeatLinks(Integer.parseInt(properties.getProperty(REPEAT_LINKS)));
        }
        if (hasProperty(properties, REPEAT_TITLES)) {
            options.setRepeatTitles(Integer.parseInt(properties.getProperty(REPEAT_TITLES)));
        }

        if (hasProperty(properties, RARE_TERM_THRESHOLD)) {
            options.setRareTermThreshold(Integer.parseInt(properties.getProperty(RARE_TERM_THRESHOLD)));
        }

        if (hasProperty(properties, BATCH_SIZE)) {
            options.setBatchSize(Integer.parseInt(properties.getProperty(BATCH_SIZE)));
        }
        if (hasProperty(properties, THREAD_COUNT)) {
            options.setThreadCount(Integer.parseInt(properties.getProperty(THREAD_COUNT)));
        }
        return options;
    }
}

package com.dreamcloud.esa_wiki.cli;

import com.dreamcloud.esa_config.cli.OptionsReader;
import com.dreamcloud.esa_wiki.annoatation.ArticleOptions;

import java.util.ArrayList;
import java.util.Properties;

public class ArticleOptionsReader extends OptionsReader {
    private static String MIN_TERMS = "article.minTerms";
    private static String MAX_TERMS = "article.maxTerms";
    private static String MIN_INLINKS = "article.minInLinks";
    private static String MIN_OUTLINKS = "article.minOutLinks";
    private static String TITLE_EXCLUSION = "article.titleExclusion";

    public ArticleOptions getOptions(Properties properties) {
        ArticleOptions options = new ArticleOptions();

        if (hasProperty(properties, MIN_TERMS)) {
            options.setMinimumTermCount(Integer.parseInt(properties.getProperty(MIN_TERMS)));
        }
        if (hasProperty(properties, MAX_TERMS)) {
            options.setMaximumTermCount(Integer.parseInt(properties.getProperty(MAX_TERMS)));
        }
        if (hasProperty(properties, MIN_INLINKS)) {
            options.setMinimumIncomingLinks(Integer.parseInt(properties.getProperty(MIN_INLINKS)));
        }
        if (hasProperty(properties, MIN_OUTLINKS)) {
            options.setMinimumOutgoingLinks(Integer.parseInt(properties.getProperty(MIN_OUTLINKS)));
        }

        for (String property: properties.stringPropertyNames()) {
            if (property.matches("^article\\.titleExclusion\\.[0-9]+\\.regexp$")) {
                options.addTitleExclusion(properties.getProperty(property));
            }
        }

        return options;
    }
}

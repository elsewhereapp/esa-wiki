package com.dreamcloud.esa_wiki;

import java.io.File;
import java.util.ArrayList;

public class IndexingOptions {
    private File sourceDirectory;
    private File outputDirectory;
    private String wikiSource;
    private ArrayList<String> stages = new ArrayList<>();
    private int repeatLinks = 0;
    private int repeatTitles = 0;
    private int rareTermThreshold = 0;
    private int threadCount = 0;
    private int batchSize = 0;

    public File getSourceDirectory() {
        return sourceDirectory;
    }

    public void setSourceDirectory(File sourceDirectory) {
        this.sourceDirectory = sourceDirectory;
    }

    public File getOutputDirectory() {
        return outputDirectory;
    }

    public void setOutputDirectory(File outputDirectory) {
        this.outputDirectory = outputDirectory;
    }

    public String getWikiSource() {
        return wikiSource;
    }

    public void setWikiSource(String wikiSource) {
        this.wikiSource = wikiSource;
    }

    public ArrayList<String> getStages() {
        return stages;
    }

    public void setStages(ArrayList<String> stages) {
        this.stages = stages;
    }

    public int getRepeatLinks() {
        return repeatLinks;
    }

    public void setRepeatLinks(int repeatLinks) {
        this.repeatLinks = repeatLinks;
    }

    public int getRepeatTitles() {
        return repeatTitles;
    }

    public void setRepeatTitles(int repeatTitles) {
        this.repeatTitles = repeatTitles;
    }

    public int getRareTermThreshold() {
        return rareTermThreshold;
    }

    public void setRareTermThreshold(int rareTermThreshold) {
        this.rareTermThreshold = rareTermThreshold;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    public int getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }
}

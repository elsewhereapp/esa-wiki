package com.dreamcloud.esa_wiki;

import com.dreamcloud.esa_config.cli.AnalyzerOptionsReader;
import com.dreamcloud.esa_config.cli.TfIdfOptionsReader;
import com.dreamcloud.esa_config.cli.VectorizationOptionsReader;
import com.dreamcloud.esa_core.analyzer.AnalyzerOptions;
import com.dreamcloud.esa_core.analyzer.EsaAnalyzer;
import com.dreamcloud.esa_core.analyzer.TokenizerFactory;
import com.dreamcloud.esa_core.vectorizer.VectorizationOptions;
import com.dreamcloud.esa_score.analysis.TfIdfAnalyzer;
import com.dreamcloud.esa_score.analysis.TfIdfOptions;
import com.dreamcloud.esa_score.analysis.TfIdfStrategyFactory;
import com.dreamcloud.esa_score.analysis.strategy.TfIdfStrategy;
import com.dreamcloud.esa_score.fs.*;
import com.dreamcloud.esa_score.score.DocumentNameResolver;
import com.dreamcloud.esa_wiki.annoatation.*;
import com.dreamcloud.esa_wiki.cli.ArticleOptionsReader;
import com.dreamcloud.esa_wiki.cli.IndexingOptionsReader;
import com.dreamcloud.esa_wiki.fs.ScoreWriter;
import com.dreamcloud.esa_wiki.fs.ScoreWriterOptions;

import com.dreamcloud.esa_wiki.word2vec.WikipediaDocumentIterator;
import com.dreamcloud.esa_wiki.word2vec.Word2VecOptions;
import com.dreamcloud.esa_wiki.word2vec.Word2VecOptionsReader;
import com.dreamcloud.esa_wiki.word2vec.ap_news.NewsArticleIterator;
import com.dreamcloud.esa_wiki.word2vec.ap_news.NewsCorpus;
import org.apache.commons.cli.*;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.wikipedia.WikipediaTokenizer;
import org.deeplearning4j.models.embeddings.learning.impl.elements.CBOW;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.documentiterator.DocumentIterator;
import org.deeplearning4j.text.sentenceiterator.StreamLineIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.util.*;

public class Main {
    private static String ANALYZE_ARTICLE = "analyze-article";

    public static void main(String[] args) {
        Options options = new Options();

        IndexingOptionsReader indexingOptionsReader = new IndexingOptionsReader();
        AnalyzerOptionsReader analyzerOptionsReader = new AnalyzerOptionsReader();
        VectorizationOptionsReader vectorizationOptionsReader = new VectorizationOptionsReader();
        ArticleOptionsReader articleOptionsReader = new ArticleOptionsReader();
        TfIdfOptionsReader tfIdfOptionsReader = new TfIdfOptionsReader();

        Option propertiesFileOption = new Option(null, "properties", true, "Path to the ESA properties file.");
        propertiesFileOption.setRequired(false);
        options.addOption(propertiesFileOption);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();

        try {
            CommandLine cli = parser.parse(options, args);
            String propertiesPath = cli.getOptionValue("properties", "./esa.properties");
            Properties properties = new Properties();
            properties.load(new FileInputStream(propertiesPath));

            System.out.println("Properties");
            System.out.println("----------------------------------------");
            properties.stringPropertyNames().stream().sorted().forEach((String property) -> {
                System.out.println(property + "\t\t=  " + properties.getProperty(property));
            });
            System.out.println("----------------------------------------\n");

            VectorizationOptions vectorOptions = vectorizationOptionsReader.getOptions(properties);
            AnalyzerOptions analyzerOptions = analyzerOptionsReader.getOptions(properties);
            TfIdfOptions tfIdfOptions = tfIdfOptionsReader.getOptions(properties);
            IndexingOptions indexingOptions = indexingOptionsReader.getOptions(properties);
            Word2VecOptions word2VecOptions = new Word2VecOptionsReader().getOptions(properties);

            //Just for Wikipedia
            Set<String> stopTokenTypes = new HashSet<>();
            stopTokenTypes.add(WikipediaTokenizer.EXTERNAL_LINK_URL);
            stopTokenTypes.add(WikipediaTokenizer.EXTERNAL_LINK);
            stopTokenTypes.add(WikipediaTokenizer.CITATION);
            analyzerOptions.setStopTokenTypes(stopTokenTypes);

            TokenizerFactory tokenizerFactory = new TokenizerFactory() {
                public Tokenizer getTokenizer() {
                    return new WikipediaTokenizer();
                }
            };
            analyzerOptions.setTokenizerFactory(tokenizerFactory);

            //For counting terms, we just want a basic analyzer that can parse wikipedia and do word counts
            AnalyzerOptions annotationAnalyzerOptions = new AnalyzerOptions();
            annotationAnalyzerOptions.setTokenizerFactory(tokenizerFactory);
            annotationAnalyzerOptions.setStopTokenTypes(stopTokenTypes);
            annotationAnalyzerOptions.display();

            ArticleOptions articleOptions = articleOptionsReader.getOptions(properties);

            ScoreWriterOptions scoreWriterOptions = new ScoreWriterOptions();
            scoreWriterOptions.setBatchSize(indexingOptions.getBatchSize());
            scoreWriterOptions.setThreadCount(indexingOptions.getThreadCount());
            scoreWriterOptions.setPreprocessor(analyzerOptions.getPreprocessor());
            scoreWriterOptions.setVectorizationOptions(vectorOptions);
            scoreWriterOptions.setAnnotationOptions(articleOptions);

            //The source file
            File wikiInputFile = new File(indexingOptions.getSourceDirectory().getPath() + "/" + indexingOptions.getWikiSource());

            //The annotated file
            File annotatedWikiInput = new File(indexingOptions.getOutputDirectory() + "/annotated.xml.bz2");

            //We now have all of the data, time to process stages
            for (String stage: indexingOptions.getStages()) {
                switch (stage) {
                    case "preprocess":
                        System.out.println("Processing Wikipedia file from " + wikiInputFile + "...");
                        File outputFile = new File(indexingOptions.getOutputDirectory() + "/preprocessed.xml.bz2");
                        File titleMapOutputFile = new File(indexingOptions.getOutputDirectory() + "/titles.xml.bz2");
                        try (WikiPreprocessor wikiPreprocessor = new WikiPreprocessor(articleOptions)) { wikiPreprocessor.preprocess(wikiInputFile, outputFile, titleMapOutputFile);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        System.out.println("Wrote preprocessed Wikipedia to " + outputFile + ", and title maps to " + titleMapOutputFile + ".");
                        break;
                    case "id-titles":
                        System.out.println("Writing ID-titles mapping file...");
                        File preprocessedWikiInput = new File(indexingOptions.getOutputDirectory() + "/preprocessed.xml.bz2");
                        if (!preprocessedWikiInput.exists()) {
                            throw new FileNotFoundException("The preprocessed file must exist at " + preprocessedWikiInput + " in order to write ID-titles.");
                        }

                        File idTitlesOutput = new File(indexingOptions.getOutputDirectory() + "/id-titles.txt");
                        try(IdTitleWriter writer = new IdTitleWriter(preprocessedWikiInput)) {
                            writer.writeTitles(idTitlesOutput);
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.exit(1);
                        }
                        System.out.println("Wrote ID-titles mapping file to " + idTitlesOutput + ".");
                        break;
                    case "count":
                        System.out.println("Annotating link and term counts...");
                        //We need the ID titles file for this step
                        File idTitlesSource = new File(indexingOptions.getOutputDirectory() + "/id-titles.txt");
                        if (!idTitlesSource.exists()) {
                            throw new FileNotFoundException("The id-titles mapping file must exist at " + idTitlesSource + " in order to count links and terms.");
                        }
                        DocumentNameResolver nameResolver = new DocumentNameResolver(idTitlesSource);

                        File titleMapSource = new File(indexingOptions.getOutputDirectory() + "/titles.xml.bz2");
                        if (!titleMapSource.exists()) {
                            throw new FileNotFoundException("The title mapping file must exist at " + titleMapSource + " in order to count links and terms.");
                        }

                        File annotatedWikiOutput = new File(indexingOptions.getOutputDirectory() + "/annotated.xml.bz2");

                        WikiLinkAndTermAnnotatorOptions wikiLinkAnnotatorOptions = new WikiLinkAndTermAnnotatorOptions();
                        wikiLinkAnnotatorOptions.minimumIncomingLinks = articleOptions.getMinimumIncomingLinks();
                        wikiLinkAnnotatorOptions.minimumOutgoingLinks = articleOptions.getMinimumOutgoingLinks();
                        wikiLinkAnnotatorOptions.minimumTerms = articleOptions.getMinimumTermCount();
                        wikiLinkAnnotatorOptions.maximumTermCount = articleOptions.getMaximumTermCount();
                        wikiLinkAnnotatorOptions.analyzer = new EsaAnalyzer(annotationAnalyzerOptions);
                        wikiLinkAnnotatorOptions.threadCount = indexingOptions.getThreadCount();
                        wikiLinkAnnotatorOptions.batchSize = indexingOptions.getBatchSize();
                        try(WikiLinkAndTermAnnotator annotator = new WikiLinkAndTermAnnotator(wikiLinkAnnotatorOptions)) {
                            annotator.annotate(new File(indexingOptions.getOutputDirectory() + "/preprocessed.xml.bz2"), titleMapSource, annotatedWikiOutput, nameResolver);
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.exit(1);
                        }
                        System.out.println("Wrote annotated file to " + annotatedWikiOutput + ".");
                        break;
                    case "repeat":
                        System.out.println("Repeating content (titles=x" + indexingOptions.getRepeatTitles() + ", links=x" + indexingOptions.getRepeatLinks() + ") in annotated Wikipedia at " + annotatedWikiInput + "...");
                        if (!annotatedWikiInput.exists()) {
                            throw new FileNotFoundException("The annotated Wikipedia file must exist at " + annotatedWikiInput + " in order to perform content repeating.");
                        }
                        File repeatWikiOutput = new File(indexingOptions.getOutputDirectory() + "/repeated.xml.bz2.tmp");
                        WikiContentRepeatOptions repeatOptions = new WikiContentRepeatOptions();
                        repeatOptions.linkRepeat = indexingOptions.getRepeatLinks();
                        repeatOptions.titleRepeat = indexingOptions.getRepeatTitles();
                        try(WikiContentRepeater repeater = new WikiContentRepeater(repeatOptions)) {
                            repeater.repeatContent(annotatedWikiInput, repeatWikiOutput);
                            //Need to overwrite the annotated file with the repeated file
                            // or we don't know what to use in the next stages.
                            Files.move(repeatWikiOutput.toPath(), annotatedWikiInput.toPath());
                        } catch (Exception e) {
                            Files.delete(repeatWikiOutput.toPath());
                            e.printStackTrace();
                            System.exit(1);
                        }
                        System.out.println("Repeated content in " + annotatedWikiInput + ".");
                        break;
                    case "rare-terms":
                        //todo: hack-stemmer
                        //We need to disable stemming as you don't do that for rare words
                        //and it's a pain to have to edit the settings just for this

                        AnalyzerOptions rareTermsOptions = analyzerOptions.clone();
                        rareTermsOptions.setUsingPorterStemmer(false);
                        rareTermsOptions.setMaximumWordLength(0);

                        File rareWordOutput = new File(indexingOptions.getOutputDirectory() + "/rare-terms.txt");
                        System.out.println("Writing rare terms (occurring in fewer than " + indexingOptions.getRareTermThreshold() + " documents...");

                        if (!annotatedWikiInput.exists()) {
                            throw new FileNotFoundException("The annotated Wikipedia file must exist at " + annotatedWikiInput + " in order to perform indexing.");
                        }

                        int rareWordThreshold = indexingOptions.getRareTermThreshold();
                        try(RareWordDictionary termCountMapper = new RareWordDictionary(new EsaAnalyzer(rareTermsOptions), rareWordThreshold, null)) {
                            termCountMapper.mapToXml(annotatedWikiInput, rareWordOutput);
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.exit(1);
                        }
                        System.out.println("Wrote rare terms to " + rareWordOutput + ".");
                        break;
                    case "word2vec":
                        System.out.println("Generating word2vec vectors...");

                        //Get the corpus
                        DocumentIterator documentIterator;
                        if (word2VecOptions.getCorpusType().equals("wiki")) {
                            System.out.println("Learning from Wikipedia...");
                            File wikiSource = word2VecOptions.getWikiSource();
                            if (!wikiSource.exists()) {
                                throw new FileNotFoundException("The annotated Wikipedia file must exist at " + wikiSource + " in order to perform word2vec.");
                            }

                            documentIterator = new WikipediaDocumentIterator(wikiSource, new EsaAnalyzer(analyzerOptions));
                        } else {
                            System.out.println("Learning from AP news...");
                            File newsSource = word2VecOptions.getNewsSource();
                            if (!newsSource.exists()) {
                                throw new FileNotFoundException("The news directory must exist at " + newsSource + " in order to perform word2vec.");
                            }
                            NewsCorpus corpus = NewsCorpus.create(newsSource);
                            documentIterator = corpus.getNewsArticleIterator(new EsaAnalyzer(analyzerOptions));
                        }

                        org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory t = new DefaultTokenizerFactory();

                        Word2Vec word2Vec;

                        //Are we training or uptraining?
                        File vectorFile = word2VecOptions.getVectorOutput();
                        if (vectorFile.exists()) {
                            System.out.println("Vector file exists...switching to uptraining mode.");
                            if (vectorFile.getName().contains("google")) {
                                word2Vec = WordVectorSerializer.readWord2VecModel(vectorFile);
                            } else {
                                word2Vec = WordVectorSerializer.readWord2VecModel(vectorFile);
                            }

                            word2Vec.setTokenizerFactory(t);
                            word2Vec.setSentenceIterator(new StreamLineIterator.Builder(documentIterator).setFetchSize(100).build());
                        } else {
                            System.out.println("Vector file does not exist...switching to generation mode...");
                            word2Vec = new Word2Vec.Builder()
                                    .minWordFrequency(indexingOptions.getRareTermThreshold())
                                    .iterations(word2VecOptions.getIterations())
                                    .layerSize(word2VecOptions.getLayerSize())
                                    .seed(42)
                                    .windowSize(word2VecOptions.getWindowSize())
                                    .iterate(documentIterator)
                                    .tokenizerFactory(t)
                                    .elementsLearningAlgorithm(word2VecOptions.getLearningAlgorithm())
                                    .build();
                        }

                        System.out.println("Fitting word2vec model....");
                        word2Vec.fit();

                        System.out.println("Writing word vectors to file....");
                        WordVectorSerializer.writeWord2VecModel(word2Vec, vectorFile);

                        //Debug vector
                        for (String word: word2VecOptions.getDebugWords()) {
                            System.out.println("Words nearest to '" + word + "':\n---");
                            for (String relative: word2Vec.wordsNearest(word, 100)) {
                                System.out.println(relative);
                            }
                            System.out.println("---");
                        }
                        break;
                    case "word2vec-debug":
                        File debugVectorFile = word2VecOptions.getVectorOutput();
                        if (!debugVectorFile.exists()) {
                            throw new FileNotFoundException("Vector file must exist at " + debugVectorFile.getPath() + " in order to perform debugging.");
                        }
                        Word2Vec debugVec = WordVectorSerializer.readWord2VecModel(debugVectorFile);
                        for (String word: word2VecOptions.getDebugWords()) {
                            System.out.println("Words nearest to '" + word + "':\n---");
                            for (String relative: debugVec.wordsNearest(word, 100)) {
                                System.out.println(relative);
                            }
                            System.out.println("---");
                        }
                        break;
                    case "index":
                        System.out.println("Indexing...");
                        if (!annotatedWikiInput.exists()) {
                            throw new FileNotFoundException("The annotated Wikipedia file must exist at " + annotatedWikiInput + " in order to perform indexing.");
                        }

                        File rareWordsInput = new File(indexingOptions.getOutputDirectory() + "/rare-terms.txt");
                        if (rareWordsInput.exists()) {
                            //add this to the stopwords
                            analyzerOptions.getStopWordsRepository().addSource(rareWordsInput);
                        }

                        Analyzer esaAnalyzer = new EsaAnalyzer(analyzerOptions);

                        System.out.println("Gathering collection information...");
                        //We need to get stats about the collection in order to compute scores
                        RareWordDictionary rareWordDictionary = new RareWordDictionary(esaAnalyzer, 0, articleOptions);
                        rareWordDictionary.parse(annotatedWikiInput);
                        TermIndex termIndex = new TermIndex(rareWordDictionary.getDocsProcessed(), rareWordDictionary.getAverageDocumentLength(), rareWordDictionary.getDocumentFrequencies());

                        System.out.println("Writing the collection...");
                        TfIdfStrategyFactory tfIdfFactory = new TfIdfStrategyFactory();
                        TfIdfStrategy tfIdfStrategy = tfIdfFactory.getStrategy(tfIdfOptions);
                        TfIdfAnalyzer tfIdfAnalyzer = new TfIdfAnalyzer(tfIdfStrategy, new EsaAnalyzer(analyzerOptions), termIndex);
                        scoreWriterOptions.setAnalyzer(tfIdfAnalyzer);

                        String indexTermPath = indexingOptions.getOutputDirectory() + "/term-index.esa";
                        String documentScoresPath = indexingOptions.getOutputDirectory() + "/document-scores.esa";
                        CollectionWriter collectionWriter = new DiskCollectionWriter(new File(indexTermPath), new File(documentScoresPath));
                        ScoreWriter writer = new ScoreWriter(annotatedWikiInput, collectionWriter, termIndex, scoreWriterOptions);
                        writer.index();

                        System.out.println("Collection written to " + indexTermPath + " and " + documentScoresPath + ".");
                        break;
                    case "stats":
                        System.out.println("Gathering collection stats...");
                        if (!annotatedWikiInput.exists()) {
                            throw new FileNotFoundException("The annotated Wikipedia file must exist at " + annotatedWikiInput + " in order to perform indexing.");
                        }

                        int rareTerms = 0; //todo: need to read this from the rare words file, which isn't really possible atm because it's incorporated into the stopwords file
                        try(ArticleStatsReader articleStatsReader = new ArticleStatsReader(new EsaAnalyzer(analyzerOptions), rareTerms)) {
                            articleStatsReader.readArticles(annotatedWikiInput);
                        } catch (ParserConfigurationException | SAXException e) {
                            e.printStackTrace();
                            System.exit(1);
                        }
                        break;
                    case "debug":
                        WikipediaDebugger debugger = new WikipediaDebugger(wikiInputFile);
                        debugger.analyze();
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            formatter.printHelp(HelpFormatter.DEFAULT_SYNTAX_PREFIX, options);
            System.exit(1);
        }
    }
}

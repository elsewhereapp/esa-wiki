package com.dreamcloud.esa_wiki.word2vec.ap_news;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class NewsDay {
    File directory;
    public int year;
    public int month;
    public int day;

    public NewsDay(File directory, int year, int month, int day) {
        this.directory = directory;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public ArrayList<String> getArticles() {
        ArrayList<String> articlePaths = new ArrayList<>();
        FilenameFilter filter = (dir, name) -> name.matches("[0-9a-f]+\\.json");
        File[] articleFiles = directory.listFiles(filter);
        if (articleFiles != null) {
            for (File articleFile: articleFiles) {
                articlePaths.add(articleFile.getAbsolutePath());
            }
        }
        return articlePaths;
    }
}

package com.dreamcloud.esa_wiki.word2vec.ap_news;

import org.apache.lucene.analysis.Analyzer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class NewsCorpus {
    private List<String> newsArticlePaths;

    public NewsCorpus(List<String> newsArticlePaths) {
        this.newsArticlePaths = newsArticlePaths;
    }

    public NewsCorpus() {
        this(new ArrayList<>());
    }

    public void addNewsArticlePath(String path) {
        this.newsArticlePaths.add(path);
    }

    public NewsArticleIterator getNewsArticleIterator(Analyzer analyzer) {
        return new NewsArticleIterator(newsArticlePaths, analyzer);
    }

    public static NewsCorpus create(File newsDirectory) {
        NewsParser parser = new NewsParser(newsDirectory);
        NewsCorpus corpus = new NewsCorpus();
        for(NewsYear year: parser.getYears()) {
            for (NewsMonth month: year.getMonths()) {
                for (NewsDay day: month.getDays()) {
                    for (String articlePath: day.getArticles()) {
                        corpus.addNewsArticlePath(articlePath);
                    }
                }
            }
        }
        return corpus;
    }
}

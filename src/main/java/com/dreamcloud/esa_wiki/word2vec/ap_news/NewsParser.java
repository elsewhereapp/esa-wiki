package com.dreamcloud.esa_wiki.word2vec.ap_news;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

public class NewsParser {
    File directory;

    public NewsParser(File directory) {
        this.directory = directory;
    }

    public ArrayList<NewsYear> getYears() {
        ArrayList<NewsYear> years = new ArrayList<>();
        FilenameFilter filter = (dir, name) -> name.matches("[0-9]{4}");
        File[] yearFiles = directory.listFiles(filter);
        if (yearFiles != null) {
            for (File yearFile: yearFiles) {
                years.add(new NewsYear(yearFile, Integer.parseInt(yearFile.getName())));
            }
        }
        return years;
    }
}

package com.dreamcloud.esa_wiki.word2vec.ap_news;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.deeplearning4j.text.documentiterator.DocumentIterator;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.safety.Safelist;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.List;

public class NewsArticleIterator implements DocumentIterator {
    private final List<String> filePathList;
    private final Analyzer analyzer;
    private Iterator<String> filePathIterator;
    private int numRead = 0;

    public NewsArticleIterator(List<String> filePathList, Analyzer analyzer) {
        this.filePathList = filePathList;
        this.analyzer = analyzer;
        reset();
    }

    @Override
    public InputStream nextDocument() {
        String UTF8_BOM = "\uFEFF";

        //Read to JSON
        byte[] encoded = new byte[0];
        try {
            encoded = Files.readAllBytes(new File(filePathIterator.next()).toPath());
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        String jsonData = new String(encoded, StandardCharsets.UTF_8);
        //Detect BOM
        if (jsonData.startsWith(UTF8_BOM)) {
            jsonData = jsonData.substring(1);
        }
        JSONObject newsData = new JSONObject(jsonData);

        //News Article
        String article = newsData.getString("body_nitf");
        article = Jsoup.clean(article, "", Safelist.none());
        article = article.replaceAll("\\?|!|(?:\\.[^0-9])", " sentenceendpunctuationmark ");
        StringBuilder tokenizedTextBuilder = new StringBuilder();
        TokenStream tokens = analyzer.tokenStream("text", article);
        CharTermAttribute termAttribute = tokens.addAttribute(CharTermAttribute.class);
        try {
            tokens.reset();
            while(tokens.incrementToken()) {
                tokenizedTextBuilder.append(termAttribute.toString()).append(' ');
            }
            tokens.close();
            String tokenizedText = tokenizedTextBuilder.toString();
            tokenizedText.replace("sentenceendpunctuationmark", "\n");

            if (numRead++ % 1000 == 0) {
                System.out.println("vec'd news article " + numRead);
            }
            return new ByteArrayInputStream(tokenizedText.getBytes(StandardCharsets.UTF_8));
        } catch(IOException e) {
            e.printStackTrace();
            System.exit(1);
            //this can't happen but we gotta have it
            return new ByteArrayInputStream(new byte[]{' '});
        }
    }

    @Override
    public boolean hasNext() {
        return filePathIterator.hasNext();
    }

    @Override
    public void reset() {
        numRead = 0;
        filePathIterator = filePathList.iterator();
    }
}

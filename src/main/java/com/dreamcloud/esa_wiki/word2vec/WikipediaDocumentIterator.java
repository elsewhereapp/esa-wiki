package com.dreamcloud.esa_wiki.word2vec;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.deeplearning4j.text.documentiterator.DocumentIterator;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.nio.charset.StandardCharsets;

public class WikipediaDocumentIterator implements DocumentIterator {
    private final File inputFile;
    private final Analyzer analyzer;
    private XMLStreamReader xmlReader;
    private int numRead = 0;

    public WikipediaDocumentIterator(File inputFile, Analyzer analyzer) throws FileNotFoundException, XMLStreamException {
        this.inputFile = inputFile;
        this.analyzer = analyzer;
        reset();
    }

    @Override
    public InputStream nextDocument() {
        while (true) {
            try {
                if (xmlReader.hasNext()) {
                    if (xmlReader.next() == XMLStreamConstants.START_ELEMENT && xmlReader.getLocalName().equals("doc")) {
                        while (xmlReader.hasNext()) {
                            if (xmlReader.next() == XMLStreamConstants.START_ELEMENT && xmlReader.getLocalName().equals("text")) {
                                String documentText = xmlReader.getElementText();
                                documentText = documentText.replaceAll("\\?|!|(?:\\.[^0-9])", " sentenceendpunctuationmark ");
                                StringBuilder tokenizedTextBuilder = new StringBuilder();
                                TokenStream tokens = analyzer.tokenStream("text", documentText);
                                CharTermAttribute termAttribute = tokens.addAttribute(CharTermAttribute.class);
                                tokens.reset();
                                while(tokens.incrementToken()) {
                                    tokenizedTextBuilder.append(termAttribute.toString()).append(' ');
                                }
                                tokens.close();
                                String tokenizedText = tokenizedTextBuilder.toString();
                                tokenizedText.replace("sentenceendpunctuationmark", "\n");

                                //read till end of doc
                                while (xmlReader.next() != XMLStreamConstants.END_ELEMENT || !xmlReader.getLocalName().equals("doc"));

                                numRead++;
                                if (numRead % 1000 == 0) {
                                    System.out.println("vec'd wiki article " + numRead);
                                }
                                return new ByteArrayInputStream(tokenizedText.getBytes(StandardCharsets.UTF_8));
                            }
                        }
                    }
                } else {
                    throw new XMLStreamException("Reached end of documents: not a problem.");
                }
            } catch (XMLStreamException|IOException e) {
                e.printStackTrace();
                byte [] document = new byte[1];
                document[0] = ' ';
                return new ByteArrayInputStream(document);
            }
        }
    }

    @Override
    public boolean hasNext() {
        try {
            return xmlReader.hasNext();
        } catch (XMLStreamException e) {
            return false;
        }
    }

    @Override
    public void reset() {
        numRead = 0;
        XMLInputFactory factory = XMLInputFactory.newInstance();
        try {
            InputStream wikiInputStream = new FileInputStream(inputFile);
            InputStream bufferedInputStream = new BufferedInputStream(wikiInputStream);
            InputStream bzipInputStream = new BZip2CompressorInputStream(bufferedInputStream, true);
            xmlReader = factory.createXMLStreamReader(bzipInputStream);
        } catch (XMLStreamException|IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}

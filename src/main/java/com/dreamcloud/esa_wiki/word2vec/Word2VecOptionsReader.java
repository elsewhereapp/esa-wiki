package com.dreamcloud.esa_wiki.word2vec;

import com.dreamcloud.esa_config.cli.OptionsReader;
import org.deeplearning4j.models.embeddings.learning.impl.elements.CBOW;
import org.deeplearning4j.models.embeddings.learning.impl.elements.SkipGram;

import java.io.File;
import java.util.Arrays;
import java.util.Properties;

public class Word2VecOptionsReader extends OptionsReader {
    private static String CORPUS_TYPE = "word2vec.corpus";
    private static String WIKI_SOURCE = "word2vec.wiki.source";
    private static String NEWS_SOURCE = "word2vec.news.source";
    private static String OUTPUT = "word2vec.output";
    private static String LAYER_SIZE = "word2vec.layerSize";
    private static String ITERATIONS = "word2vec.iterations";
    private static String WINDOW_SIZE = "word2vec.windowSize";
    private static String ALGORITHM = "word2vec.algorithm";
    private static String DEBUG_WORDS = "word2vec.debug.words";

    public Word2VecOptions getOptions(Properties properties) {
        Word2VecOptions options = new Word2VecOptions();

        if (hasProperty(properties, CORPUS_TYPE)) {
            options.setCorpusType(properties.getProperty(CORPUS_TYPE));
        }
        if (hasProperty(properties, WIKI_SOURCE)) {
            options.setWikiSource(new File(properties.getProperty(WIKI_SOURCE)));
        }
        if (hasProperty(properties, NEWS_SOURCE)) {
            options.setNewsSource(new File(properties.getProperty(NEWS_SOURCE)));
        }
        if (hasProperty(properties, OUTPUT)) {
            options.setVectorOutput(new File(properties.getProperty(OUTPUT)));
        }
        if (hasProperty(properties, LAYER_SIZE)) {
            options.setLayerSize(Integer.parseInt(properties.getProperty(LAYER_SIZE)));
        }
        if (hasProperty(properties, WINDOW_SIZE)) {
            options.setWindowSize(Integer.parseInt(properties.getProperty(WINDOW_SIZE)));
        }
        if (hasProperty(properties, ITERATIONS)) {
            options.setIterations(Integer.parseInt(properties.getProperty(ITERATIONS)));
        }
        if (hasProperty(properties, ALGORITHM)) {
            String algorithm = properties.getProperty(ALGORITHM);
            switch(algorithm) {
                case "cbow":
                    options.setLearningAlgorithm(new CBOW());
                    break;
                case "skip":
                    options.setLearningAlgorithm(new SkipGram());
                    break;
                default:
                    throw new IllegalArgumentException("Invalid learning algorithm '" + algorithm + "'");
            }
        }
        if (hasProperty(properties, DEBUG_WORDS)) {
            String[] debugWords = properties.getProperty(DEBUG_WORDS).split(",");
            options.setDebugWords(Arrays.asList(debugWords));
        }
        return options;
    }
}

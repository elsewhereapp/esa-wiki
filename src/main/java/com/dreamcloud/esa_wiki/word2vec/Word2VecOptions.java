package com.dreamcloud.esa_wiki.word2vec;

import org.deeplearning4j.models.embeddings.learning.ElementsLearningAlgorithm;
import org.deeplearning4j.models.embeddings.learning.impl.elements.CBOW;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Word2VecOptions {
    private String corpusType;
    private File wikiSource;
    private File newsSource;
    private File vectorOutput;
    private int layerSize = 100;
    private int iterations = 1;
    private int windowSize = 5;
    private ElementsLearningAlgorithm learningAlgorithm = new CBOW();
    private List<String> debugWords = new ArrayList<>();

    public File getVectorOutput() {
        return vectorOutput;
    }

    public void setVectorOutput(File vectorOutput) {
        this.vectorOutput = vectorOutput;
    }

    public String getCorpusType() {
        return corpusType;
    }

    public void setCorpusType(String corpusType) {
        this.corpusType = corpusType;
    }

    public File getWikiSource() {
        return wikiSource;
    }

    public void setWikiSource(File wikiSource) {
        this.wikiSource = wikiSource;
    }

    public File getNewsSource() {
        return newsSource;
    }

    public void setNewsSource(File newsSource) {
        this.newsSource = newsSource;
    }

    public int getLayerSize() {
        return layerSize;
    }

    public void setLayerSize(int layerSize) {
        this.layerSize = layerSize;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public int getWindowSize() {
        return windowSize;
    }

    public void setWindowSize(int windowSize) {
        this.windowSize = windowSize;
    }

    public ElementsLearningAlgorithm getLearningAlgorithm() {
        return learningAlgorithm;
    }

    public void setLearningAlgorithm(ElementsLearningAlgorithm learningAlgorithm) {
        this.learningAlgorithm = learningAlgorithm;
    }

    public List<String> getDebugWords() {
        return debugWords;
    }

    public void setDebugWords(List<String> debugWords) {
        this.debugWords = debugWords;
    }
}

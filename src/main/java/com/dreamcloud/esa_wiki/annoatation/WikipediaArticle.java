package com.dreamcloud.esa_wiki.annoatation;

public class WikipediaArticle {
    public int id;
    public String title;
    public String text;
    public Integer incomingLinks;
    public Integer outgoingLinks;
    public Integer terms;


    public boolean canIndex(ArticleOptions options) {
        return !(
                (options.getMinimumIncomingLinks() > 0 && incomingLinks != null && incomingLinks < options.getMinimumIncomingLinks())
                || (options.getMinimumOutgoingLinks() > 0 && outgoingLinks != null && outgoingLinks < options.getMinimumOutgoingLinks())
                || (options.getMinimumTermCount() > 0 && terms != null && terms < options.getMinimumTermCount())
                || (options.getMaximumTermCount() > 0 && terms != null && terms > options.getMaximumTermCount())
        );
    }
}
